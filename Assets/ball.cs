﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour {

    Rigidbody2D rb;
    

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        HandleDrop();

    }

    private void HandleDrop()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.simulated = true;
            GetComponent<moveController>().enabled = false;
        }
    }

    
}
