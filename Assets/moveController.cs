﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveController : MonoBehaviour
{
    Rigidbody2D rb;
    [SerializeField] float speed = 0.05f;
    [SerializeField] GameManager GM;
    [SerializeField] UI_Manager UIM;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        UIM = GameObject.Find("UI_Manager").GetComponent<UI_Manager>();
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleMove();
    }

    void HandleMove()
    {
        Vector3 pos = transform.position;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position = new Vector3(pos.x - speed, pos.y, pos.z);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = new Vector3(pos.x + speed, pos.y, pos.z);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("25") || collision.gameObject.CompareTag("50") || collision.gameObject.CompareTag("100"))
        {
            HandleEndTurn(collision.gameObject.tag);
        }
    }

    public void HandleEndTurn(string tag)
    {

        GM.scoreTally(tag);
        UIM.DisplayGEP();
        UIM.scoreTallyUI(tag);
    }

    public void Reset()
    {
        rb.simulated = false;
        rb.velocity = Vector2.zero;
        transform.position = new Vector3(2.6f, 9, 0);
        GetComponent<moveController>().enabled = true;
    }

}
