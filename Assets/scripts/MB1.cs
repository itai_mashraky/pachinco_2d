﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MB1: MonoBehaviour {

    bool canJump = true;

    public float x{
        get
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            return t.position.x;
        }
        set
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            t.position = 
                new Vector3(value, t.position.y, t.position.z);
        }
    }

    public float y
    {
        get
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            return t.position.y;
        }
        set
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            t.position =
                 new Vector3(t.position.x, value,  t.position.z);
        }
    }

    public float rotation
    {
        get
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            return t.rotation.z;
        }
        set
        {
            transform.Rotate(new Vector3(0, 0, value));
        }
    }

    public float depth
    {
        get
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            return t.position.z;
        }
        set
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            t.position =
                new Vector3(t.position.x, t.position.y, value);
        }

    }

    public float scale
    {
        get
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            return t.localScale.x;
        }
        set
        {
            Transform t = gameObject.GetComponentInParent<Transform>();
            t.localScale =
                new Vector3(value, value, value);
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void print(string str){
        Debug.Log(str);
    }

    public void jump(float jumpForse = 8f)
    {
        if(canJump == false)
        {
            return;
        }
        canJump = false;
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(rb.velocity.x, jumpForse);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        handleCollision(collision);
    }

    internal void handleCollision(Collision2D collision)
    {
        Collider2D col = collision.gameObject.GetComponent<Collider2D>();
        if (col.bounds.max.y < y)
        {
            canJump = true;

        }
    }

    internal GameObject copyToPosition(float x1, float y1)
    {
        return Instantiate(this, new Vector3(x1, y1, 0), Quaternion.identity).gameObject;

    }
}
