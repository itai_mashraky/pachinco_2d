﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{

    [SerializeField] Text currGPScore, currGPScoreOnEGP, totalScoreEGP;
    [SerializeField] GameManager GM;
    /// <summary>
    /// GameEnd Panel
    /// </summary>
    [SerializeField] GameObject GEPanel;
    [SerializeField] private moveController moveControllerScript;



    // Use this for initialization
    void Start()
    {
        moveControllerScript = GameObject.Find("coin").GetComponent<moveController>();
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
        currGPScore.text = "score: 0";
        currGPScoreOnEGP.text = "you got " + 0 + " points";
        totalScoreEGP.text = "total Score: " + 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisplayGEP()
    {
        currGPScore.text = "";
        GEPanel.SetActive(true);
    }

    public void scoreTallyUI(string tag)
    {
        if (tag.Equals("25"))
        {
            currGPScoreOnEGP.text = "you got " + tag + " points";
            totalScoreEGP.text = "total score: " + GM.GetTotalScore();
        }
        else if (tag.Equals("50"))
        {
            currGPScoreOnEGP.text = "you got " + tag + " points";
            totalScoreEGP.text = "total score: " + GM.GetTotalScore();
        }
        else if (tag.Equals("100"))
        {
            currGPScoreOnEGP.text = "you got " + tag + " points";
            totalScoreEGP.text = "total score: " + GM.GetTotalScore();
        }
        
    }

    public void ResumeGame()
    {
        currGPScore.text = "Current Score: " + GM.GetTotalScore();
        GEPanel.SetActive(false);
        moveControllerScript.Reset();
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
