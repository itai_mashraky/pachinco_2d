﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private int currGPScore = 0;
    [SerializeField] private int totalScore = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void scoreTally(string tag)
    {
        if (tag.Equals("25"))
        {
            currGPScore = 25;
            totalScore = totalScore + currGPScore;
            Debug.Log("i hit tag: " + tag);
        }
        else if (tag.Equals("50"))
        {
            currGPScore = 50;
            totalScore = totalScore + currGPScore;
            Debug.Log("i hit tag: " + tag);
        }
        else if (tag.Equals("100"))
        {
            currGPScore = 100;
            totalScore = totalScore + currGPScore;
            Debug.Log("i hit tag: " + tag);
        }
    }

    public int GetTotalScore()
    {
        return this.totalScore;
    }
}
